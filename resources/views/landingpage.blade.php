<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="/css/main.css">

    <title>Happy Life</title>
</head>
<body class="font-sans bg-orange-200 text-white">
    <nav class="border-b border-orange-600">
        <div class="container mx-auto flex items-center justify-between px-4 py-6">
            <ul class="flex items-center">
                <li>
                    <a class="uppercase tracking-wider text-lg font-bold text-red-300" href="#">Happy Life</a>
                </li>
                <li class="ml-16">
                    <a href="#">Vendor</a>
                </li>
                <li class="ml-6">
                    <a href="#">Vendor</a>
                </li>
                <li class="ml-6">
                    <a href="#">Vendor</a>
                </li>
                <li class="ml-6">
                    <a href="#">Vendor</a>
                </li>
            </ul>
            <div class="flex items-center">
                <a href="#" class="mr-6">Button</a>
                <a href="#">Button</a>
            </div>
        </div>
    </nav>
    <section class="container mx-auto flex items-center py-20">
        <div class="w-1/2 p-24 text-right">
            <h1 class="text-6xl font-bold leading-none">
                E-Commerce Wedding Platform
            </h1>
            <p class="leading-relaxed mt-6 text-gray-600">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia provident iure rem harum asperiores quas porro doloremque, vitae facere ducimus aut impedit culpa ipsam vero amet! Ex quis cupiditate et?
            </p>
            <a href="#" class="bg-red-300 text-white font-semibold py-1 px-8 inline-block mt-6">Sign Up</a>
        </div>
        <div class="w-1/2 mr-8">
            <img class="max-w-4xl" src="/img/bg.jpg" alt="wedding">
        </div>
    </section>
</body>
</html>
