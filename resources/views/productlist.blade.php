<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="/css/main.css">
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>

    <title>Product Deals</title>
</head>
<body class="relative font-poppin min-h-full">
    <header class="position-fixed top-0 box-border bg-green-300 w-full h-16">
        <div class="border-b border-green-900 px-3 pt-1 min-w-full m-auto h-16 flex justify-between items-center">
            <div class="flex mr-4">
                <a class="inline-block h-8 font-semibold text-lg" href="#">Happy Life</a>
            </div>
            <div class="flex-auto min-w-0">
                <form>
                    <div class="h-8 relative">
                        <input class="border-none box-border h-8 rounded pr-8 pl-10 w-full bg-green-100 font-normal text-xs tracking-wider outline-none" type="text">
                    </div>
                </form>
            </div>
            <div class="flex-initial ml-16">
                <ul class="leading-4 tracking-wider flex items-center">
                    <li class="float-left w-6 h-6 overflow-hidden rounded-full">
                        <img src="/img/ava.jpg" alt="avatar">
                    </li>
                    <li class="flex items-center float-left relative h-10">
                        <a class="text-white -mr-1 text-sm capitalize overflow-hidden whitespace-no-wrap block float-left px-2 py-2 tracking-widest" href="#">Rizky Firmansyah</a>
                        <span class="iconify inline-block ml-1 text-white" data-inline="false" data-icon="ls:dropdown" style="font-size: 7px; text-overflow: ellipsis"></span>
                    </li>
                    <li class="mx-5 border-r border-solid border-gray-300 h-6 float-left"></li>
                    <li class="relative mr-4 mt-0 float-left">
                        <a class="cursor-pointer no-underline" href="#">
                            <span class="iconify text-lg text-white" data-inline="false" data-icon="bx:bxs-shopping-bag-alt"></span>
                        </a>
                    </li>
                    <li class="relative mr-4 mt-0 float-left">
                        <a class="cursor-pointer no-underline" href="#">
                            <span class="iconify text-lg text-white" data-inline="false" data-icon="clarity:bell-solid-badged"></span>
                        </a>
                    </li>
                    <li class="relative mr-0 mt-0 float-left">
                        <a class="cursor-pointer no-underline" href="#">
                            <span class="iconify text-white" data-inline="false" data-icon="ant-design:message-filled"></span>
                        </a>
                    </li>
                    <li class="mx-5 border-r border-solid border-gray-300 h-6 float-left"></li>
                    <li class="relative float-left">
                        <a class="cursor-pointer no-underline" href="#">
                            <div class="text-white leading-6 text-xs font-bold uppercase">
                                <div class="float-left w-6 h-6 overflow-hidden rounded-full inline-block mr-1">
                                    <img src="/img/ava.jpg" alt="avatar">
                                </div>
                                EN
                                <span class="iconify inline-block ml-1" data-inline="false" data-icon="ls:dropdown" style="font-size: 7px; text-overflow: ellipsis"></span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="h-12 w-full px-3 inline-block bg-white shadow-md border-b border-solid border-gray-500">
            <div>
                <div class="flex w-10/12 tracking-widest leading-5 text-sm float-left">
                    <a class="ml-0 py-4 pt-3 mr-4 leading-5 text-gray-700 hover:text-green-300" href="#">Home</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Store</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Inspirations</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Vendors</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Events</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Blog</a>
                    <a class="py-4 pt-3 mx-4 leading-5 text-gray-700 hover:text-green-300" href="#">Get Our App</a>
                </div>
                <div class="block float-right text-sm font-medium tracking-widest leading-5 text-black rounded-md bg-white shadow-xs my-1 px-6 py-2">
                    <a class="block float-left cursor-pointer no-underline" href="#">
                        <div class="flex justify-center items-center cursor-pointer text-black overflow-hidden">
                            <span class="iconify w-3 h-3" data-inline="false" data-icon="fa-solid:list-ul"></span>
                            <span class="ml-2 text-black">My Order</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="bg-white mt-12">
        <div class="m-0 px-20 w-full">
            <div class="breadcrumb-container">
                <ul class="overflow-hidden py-5 cursor-pointer text-gray-500 list-none">
                    <li class="float-left text-gray-500 text-xs leading-4 mr-6 relative">
                        <a class="cursor-pointer no-underline text-green-300 hover:underline" href="#">Home</a>
                    </li>
                    <li class="float-left text-gray-500 text-xs leading-4 mr-6 relative">
                        <a class="cursor-pointer no-underline text-green-300 hover:underline" href="#">Store</a>
                    </li>
                    <li class="float-left text-gray-500 text-xs leading-4 mr-6 relative">
                        <h1>Flash Deals</h1>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="relative">
        <div class="px-20 w-full max-w-full">
            <div class="relative overflow-visible flex items-center pb-8">
                <div class="flex items-center float-left mb-0 w-full">
                    <h2 class="mr-2 text-black text-3xl tracking-wide leading-10 inline-block">
                        <i class="icon-thunder"></i>Flash Deals
                    </h2>
                    <div class="flex items-center justify-between py-3 px-5 ml-8 rounded-lg bg-green-300">
                        <div class="text-xs text-black leading-3 tracking-wide w-16 mr-2">
                            Ends In
                        </div>
                        <div class="flex items-center mr-1">
                            <div class="flex items-center justify-center bg-green-600 w-8 flex-col mx-1 py-1 pl-px pr-px rounded-md">
                                <span class="text-white font-semibold text-base tracking-wide leading-4">09</span>
                                <span class="text-white text-center text-xs">days</span>
                            </div>
                            <div class="flex items-center justify-center bg-green-600 w-8 flex-col mx-1 py-1 pl-px pr-px rounded-md">
                                <span class="text-white font-semibold text-base tracking-wide leading-4">09</span>
                                <span class="text-white text-center text-xs">days</span>
                            </div>
                            <div class="flex items-center justify-center bg-green-600 w-8 flex-col mx-1 py-1 pl-px pr-px rounded-md">
                                <span class="text-white font-semibold text-base tracking-wide leading-4">09</span>
                                <span class="text-white text-center text-xs">days</span>
                            </div>
                            <div class="flex items-center justify-center bg-green-600 w-8 flex-col mx-1 py-1 pl-px pr-px rounded-md">
                                <span class="text-white font-semibold text-base tracking-wide leading-4">09</span>
                                <span class="text-white text-center text-xs">days</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pb-20">
                <div class="grid grid-custom row-gap-12 col-gap-5">
                    <a class="block box-border relative rounded-lg bg-white shadow-md cursor-pointer no-underline" href="#">
                        <div class="overflow-hidden h-12">
                            <div class="mt-3 mx-4 mb-0">
                                Header Content
                            </div>
                        </div>
                        <div class="relative">
                            <div class="relative overflow-hidden h-40 flex items-center justify-center">
                                <img class="object-cover h-full w-full" style="min-height: 10rem;" src="/img/qq.jpg" alt="foto">
                                <div class="absolute w-full h-10 bottom-0 left-0 linear-gradient">
                                    <div class="absolute text-white text-xs leading-3 tracking-widest capitalize" style="left: 12px; bottom: 9px;">
                                        <span class="icon-location3"></span>
                                        Jakarta, ID
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="flex items-center overflow-hidden h-12 border-b border-solid border-gray-500">
                                <div class="flex flex-shrink flex-col">
                                    <del class="text-gray-700 text-xs leading-3 block">
                                        IDR 200.000
                                    </del>
                                    <strong class="text-black text-base leading-4 block">
                                        IDR 50.000
                                    </strong>
                                </div>
                            </div>
                            <div class="text-gray-700 text-xs leading-3 pt-1 pb-3">
                                <span><strong>0% Installment</strong> up to 24 months</span>
                            </div>
                        </div>
                    </a>
                    <a class="block box-border relative rounded-lg bg-white shadow-md cursor-pointer no-underline" href="#">
                        <div class="overflow-hidden h-12">
                            <div class="mt-3 mx-4 mb-0">
                                Header Content
                            </div>
                        </div>
                        <div class="relative">
                            <div class="relative overflow-hidden h-40 flex items-center justify-center">
                                <img class="object-cover h-full w-full" style="min-height: 10rem;" src="/img/qq.jpg" alt="foto">
                                <div class="absolute w-full h-10 bottom-0 left-0 linear-gradient">
                                    <div class="absolute text-white text-xs leading-3 tracking-widest capitalize" style="left: 12px; bottom: 9px;">
                                        <span class="icon-location3"></span>
                                        Jakarta, ID
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="flex items-center overflow-hidden h-12 border-b border-solid border-gray-500">
                                <div class="flex flex-shrink flex-col">
                                    <del class="text-gray-700 text-xs leading-3 block">
                                        IDR 200.000
                                    </del>
                                    <strong class="text-black text-base leading-4 block">
                                        IDR 50.000
                                    </strong>
                                </div>
                            </div>
                            <div class="text-gray-700 text-xs leading-3 pt-1 pb-3">
                                <span><strong>0% Installment</strong> up to 24 months</span>
                            </div>
                        </div>
                    </a>
                    <a class="block box-border relative rounded-lg bg-white shadow-md cursor-pointer no-underline" href="#">
                        <div class="overflow-hidden h-12">
                            <div class="mt-3 mx-4 mb-0">
                                Header Content
                            </div>
                        </div>
                        <div class="relative">
                            <div class="relative overflow-hidden h-40 flex items-center justify-center">
                                <img class="object-cover h-full w-full" style="min-height: 10rem;" src="/img/qq.jpg" alt="foto">
                                <div class="absolute w-full h-10 bottom-0 left-0 linear-gradient">
                                    <div class="absolute text-white text-xs leading-3 tracking-widest capitalize" style="left: 12px; bottom: 9px;">
                                        <span class="icon-location3"></span>
                                        Jakarta, ID
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="flex items-center overflow-hidden h-12 border-b border-solid border-gray-500">
                                <div class="flex flex-shrink flex-col">
                                    <del class="text-gray-700 text-xs leading-3 block">
                                        IDR 200.000
                                    </del>
                                    <strong class="text-black text-base leading-4 block">
                                        IDR 50.000
                                    </strong>
                                </div>
                            </div>
                            <div class="text-gray-700 text-xs leading-3 pt-1 pb-3">
                                <span><strong>0% Installment</strong> up to 24 months</span>
                            </div>
                        </div>
                    </a>
                    <a class="block box-border relative rounded-lg bg-white shadow-md cursor-pointer no-underline" href="#">
                        <div class="overflow-hidden h-12">
                            <div class="mt-3 mx-4 mb-0">
                                Header Content
                            </div>
                        </div>
                        <div class="relative">
                            <div class="relative overflow-hidden h-40 flex items-center justify-center">
                                <img class="object-cover h-full w-full" style="min-height: 10rem;" src="/img/qq.jpg" alt="foto">
                                <div class="absolute w-full h-10 bottom-0 left-0 linear-gradient">
                                    <div class="absolute text-white text-xs leading-3 tracking-widest capitalize" style="left: 12px; bottom: 9px;">
                                        <span class="icon-location3"></span>
                                        Jakarta, ID
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="flex items-center overflow-hidden h-12 border-b border-solid border-gray-500">
                                <div class="flex flex-shrink flex-col">
                                    <del class="text-gray-700 text-xs leading-3 block">
                                        IDR 200.000
                                    </del>
                                    <strong class="text-black text-base leading-4 block">
                                        IDR 50.000
                                    </strong>
                                </div>
                            </div>
                            <div class="text-gray-700 text-xs leading-3 pt-1 pb-3">
                                <span><strong>0% Installment</strong> up to 24 months</span>
                            </div>
                        </div>
                    </a>
                    <a class="block box-border relative rounded-lg bg-white shadow-md cursor-pointer no-underline" href="#">
                        <div class="overflow-hidden h-12">
                            <div class="mt-3 mx-4 mb-0">
                                Header Content
                            </div>
                        </div>
                        <div class="relative">
                            <div class="relative overflow-hidden h-40 flex items-center justify-center">
                                <img class="object-cover h-full w-full" style="min-height: 10rem;" src="/img/qq.jpg" alt="foto">
                                <div class="absolute w-full h-10 bottom-0 left-0 linear-gradient">
                                    <div class="absolute text-white text-xs leading-3 tracking-widest capitalize" style="left: 12px; bottom: 9px;">
                                        <span class="icon-location3"></span>
                                        Jakarta, ID
                                    </div>
                                </div>
                            </div>
                            <div class="discount-banner">
                                <span class="text-xs leading-3 block">Save</span>
                                <div><strong class="text-sm leading-4">150rb</strong></div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="flex items-center overflow-hidden h-12 border-b border-solid border-gray-500">
                                <div class="flex flex-shrink flex-col">
                                    <del class="text-gray-700 text-xs leading-3 block">
                                        IDR 200.000
                                    </del>
                                    <strong class="text-black text-base leading-4 block">
                                        IDR 50.000
                                    </strong>
                                </div>
                            </div>
                            <div class="text-gray-700 text-xs leading-3 pt-1 pb-3">
                                <span><strong>0% Installment</strong> up to 24 months</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>